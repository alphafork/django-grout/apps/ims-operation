import datetime

from ims_base.serializers import BaseModelSerializer
from rest_framework import serializers

from .models import Operation, OperationLog


class OperationBaseSerializer(BaseModelSerializer):
    is_authorized = serializers.BooleanField()

    class Meta(BaseModelSerializer.Meta):
        model = OperationLog
        fields = [
            "is_authorized",
        ]

    def create(self, validated_data):
        validated_data.pop("is_authorized")
        operation = Operation.objects.get(name=self.context["operation"])
        initiated_time = datetime.datetime.now()
        status, log_entry = self.run_operation()
        completed_time = datetime.datetime.now()
        validated_data.update(
            {
                "authorized_user": self.context["request"].user,
                "operation": operation,
                "initiated_time": initiated_time,
                "completed_time": completed_time,
                "status": status,
                "log_entry": log_entry,
            }
        )
        return super().create(validated_data)

    def run_operation(self):
        status = None
        log_entry = None
        return status, log_entry

    def to_representation(self, instance):
        return {
            "operation": instance.operation.id,
            "operation_name": instance.operation.__str__(),
            "authorized_user": instance.authorized_user.id,
            "authorized_user_name": instance.authorized_user.get_full_name(),
            "initiated_time": instance.initiated_time,
            "completed_time": instance.completed_time,
            "status": dict(instance.OPERATION_STATUS)[instance.status],
            "log_entry": instance.log_entry,
        }


class OperationLogSerializer(BaseModelSerializer):
    class Meta(BaseModelSerializer.Meta):
        extra_meta = {
            "authorized_user": {
                "related_model_url": "/user/account",
            }
        }

    def to_representation(self, instance):
        data = super().to_representation(instance)
        return {
            **data,
            **{
                "operation_name": instance.__str__(),
                "authorized_user": instance.authorized_user.id,
                "authorized_user_name": instance.authorized_user.get_full_name(),
            },
        }
