from django.contrib.auth.models import Group, User
from django.db import models
from ims_base.models import AbstractBaseDetail, AbstractLog


class Operation(AbstractBaseDetail):
    app_label = models.CharField(max_length=100)
    auth_groups = models.ManyToManyField(Group)

    def __str__(self) -> str:
        return self.name + " | " + self.app_label


class OperationLog(AbstractLog):
    OPERATION_STATUS = [
        ("su", "success"),
        ("fa", "failed"),
    ]
    operation = models.ForeignKey(Operation, on_delete=models.CASCADE)
    authorized_user = models.ForeignKey(User, on_delete=models.CASCADE)
    initiated_time = models.DateTimeField(null=True, blank=True)
    completed_time = models.DateTimeField(null=True, blank=True)
    log_entry = models.TextField(null=True, blank=True)
    status = models.CharField(max_length=2, choices=OPERATION_STATUS, default="st")

    def __str__(self) -> str:
        return f"{self.operation.name} {self.initiated_time.date()}"
