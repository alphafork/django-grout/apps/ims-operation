from django.apps import AppConfig


class IMSOperationConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "ims_operation"

    model_strings = {
        "OPERATION": "Operation",
        "OPERATION_LOG": "OperationLog",
    }
