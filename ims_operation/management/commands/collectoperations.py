from django.apps import apps
from django.contrib.auth.models import Group
from django.core.management.base import BaseCommand, CommandError
from django.utils.module_loading import import_string

from ims_operation.models import Operation


class Command(BaseCommand):
    help = "Collects operations from apps."

    def add_arguments(self, parser):
        parser.add_argument("app_labels", nargs="+", type=str)

    def handle(self, *args, **options):
        for app_label in options["app_labels"]:
            try:
                app = apps.get_app_config(app_label)
                operation_apis = app.operation_apis
            except LookupError:
                raise CommandError('App "%s" does not exist' % app_label)
            except AttributeError:
                pass
            else:
                for operation_api in operation_apis:
                    try:
                        api = import_string(app.label + ".apis." + operation_api)
                    except ImportError:
                        raise CommandError('API "%s" does not exist' % operation_api)
                    else:
                        operation = getattr(api, "operation", None)
                        if not operation:
                            raise CommandError(
                                """"%s" does not have operation attribute"""
                                % operation_api
                            )
                        auth_groups = getattr(api, "auth_groups", None)
                        if not auth_groups:
                            raise CommandError(
                                """"%s" does not have auth_groups attribute"""
                                % operation_api
                            )

                        operation = Operation.objects.get_or_create(
                            name=operation,
                            app_label=app_label,
                        )[0]

                        for group_str in auth_groups:
                            group = Group.objects.filter(name=group_str)
                            if not group.exists():
                                operation.delete()
                                raise CommandError(
                                    'Group "%s" does not exist' % group_str
                                )
                            operation.auth_groups.add(group[0])
                        operation.save()
                        self.stdout.write(
                            self.style.SUCCESS(
                                """Added operations for "%s" app.""" % app_label
                            )
                        )
