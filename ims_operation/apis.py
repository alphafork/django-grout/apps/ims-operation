from ims_base.apis import BaseAPIViewSet
from rest_framework.permissions import BasePermission

from ims_operation.models import Operation, OperationLog
from ims_operation.serializers import OperationBaseSerializer


class OperationPermission(BasePermission):
    def has_permission(self, request, view):
        auth_groups = Operation.objects.get(name=view.operation).auth_groups.all()

        for group in request.user.groups.all():
            if group in auth_groups:
                return True
        return False


class OperationBaseAPI(BaseAPIViewSet):
    operation = None
    auth_groups = None
    serializer_class = OperationBaseSerializer
    model_class = OperationLog
    permission_classes = BaseAPIViewSet.permission_classes + [OperationPermission]

    def get_serializer_context(self):
        context = super().get_serializer_context()
        context["operation"] = self.operation
        return context
